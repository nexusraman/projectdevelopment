## How to Download and execute project

1. Clone or download this repository.
2. Create a personal access token with all Devices scopes selected. Copy or store this token in a secure place.
3. Create an environment variable named SMARTTHINGS_CLI_TOKEN, and set its value to your personal access token obtained in step 2).
4. Install the dependencies: npm install -i.
5. Run the application: npm start 

## License 
License is provided by Licence.md by Apache 2.0
